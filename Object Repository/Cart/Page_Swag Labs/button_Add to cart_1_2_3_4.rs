<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add to cart_1_2_3_4</name>
   <tag></tag>
   <elementGuidId>70d857ac-6c22-41a3-b918-f5bd0ae459ad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='add-to-cart-sauce-labs-onesie']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#add-to-cart-sauce-labs-onesie</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8f9b0cda-eb00-4a65-98a3-a1e7274ce1e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn_primary btn_small btn_inventory</value>
      <webElementGuid>e4a01503-7d0f-437d-970d-491bf09c1209</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-onesie</value>
      <webElementGuid>6eafd918-d901-4f8d-b2b9-2b8f2396dba4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-onesie</value>
      <webElementGuid>37c7919e-669c-4946-9113-de41b22a6a48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-onesie</value>
      <webElementGuid>bd6caa02-1696-4732-80bf-f6654a6618fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>0c71e27e-911a-4931-9aa7-6ddc0d3d3061</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-to-cart-sauce-labs-onesie&quot;)</value>
      <webElementGuid>d34b8ed9-01db-488a-8c13-1a5f2aa9006a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='add-to-cart-sauce-labs-onesie']</value>
      <webElementGuid>b8b7954c-6b51-4a43-9728-5ed50b7c7bb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='inventory_container']/div/div[5]/div[2]/div[2]/button</value>
      <webElementGuid>4b564603-e886-418a-ac7d-dbdb9221726e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$7.99'])[1]/following::button[1]</value>
      <webElementGuid>3196dcb8-ca83-48bb-ae0e-c22f133af8fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Onesie'])[1]/following::button[1]</value>
      <webElementGuid>8ec127f9-68e6-4968-ad46-62ffe8056f59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test.allTheThings() T-Shirt (Red)'])[1]/preceding::button[1]</value>
      <webElementGuid>55025417-dd6b-45ea-aab4-20b06381c441</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$15.99'])[2]/preceding::button[1]</value>
      <webElementGuid>074afe65-7db5-4ee9-9565-6b06d6a462d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/div[2]/button</value>
      <webElementGuid>7035a73e-7a9a-412f-8330-bae2cf6a0c55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'add-to-cart-sauce-labs-onesie' and @name = 'add-to-cart-sauce-labs-onesie' and (text() = 'Add to cart' or . = 'Add to cart')]</value>
      <webElementGuid>68dddaf7-116a-4303-93ab-a2e6d84f0570</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
