<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_standard_userlocked_out_userproblem_u_db77ac (4)</name>
   <tag></tag>
   <elementGuidId>da650961-c998-420d-ba78-99a6c9f71183</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='user-name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#user-name</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f64faa59-11fc-4d6f-87b1-ab29c3439bd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input_error form_input</value>
      <webElementGuid>02e47393-0743-4633-8d09-f6f184b59e8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Username</value>
      <webElementGuid>ef23912e-6b16-49a7-9b40-d34ed48e0bf6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>2a99d3ad-f7ce-4c25-b248-1affbb638832</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>348d7f86-894b-4088-a60f-69707e82664d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user-name</value>
      <webElementGuid>eb00ca03-9eb2-4a78-a620-ca4592a83abf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user-name</value>
      <webElementGuid>933332a7-f2fc-4012-998f-eb96423187e0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocorrect</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>800b4019-4f0c-4aa0-b946-d63de0fe6a05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocapitalize</name>
      <type>Main</type>
      <value>none</value>
      <webElementGuid>be34a7a3-9b75-4e18-8e5f-6b81cd5444fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user-name&quot;)</value>
      <webElementGuid>ee33caf3-95a0-4ffd-bd93-f20d0ed6aa29</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='user-name']</value>
      <webElementGuid>556d1631-0711-4a07-9bf2-5717d907fdc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/div/input</value>
      <webElementGuid>6470242e-8d15-4906-89b5-a49458497164</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>01ff4211-f74a-4fe2-9bbe-f0e5be3ad980</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Username' and @type = 'text' and @id = 'user-name' and @name = 'user-name']</value>
      <webElementGuid>3b1a20c9-3693-4209-a373-1ee4ac2eb566</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
