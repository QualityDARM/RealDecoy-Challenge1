<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_standard_userlocked_out_userproblem_u_0dff71 (4)</name>
   <tag></tag>
   <elementGuidId>f87be859-5270-4f4b-8a51-c2032b5f75a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='login-button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#login-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>7feb95b0-4405-4d9b-9fb4-b465a40127e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>b75e498b-247d-4cff-a8d4-26ef0b7e6ab9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button btn_action</value>
      <webElementGuid>541158f5-9a17-42e6-90f1-57f310c5b117</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>0799427a-b789-4aeb-b892-67ab7f6a1dff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>35b517df-2f55-4cd7-b582-a2cf53f79c7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>8ff79864-4813-48a3-8702-a2dcc1714abe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Login</value>
      <webElementGuid>997deac5-ec9d-4ed4-a959-bbabd275449f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login-button&quot;)</value>
      <webElementGuid>3af456dd-a20b-4220-a946-7d2e900cba3e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='login-button']</value>
      <webElementGuid>83698896-3831-47a3-be07-6133675c5dc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/input</value>
      <webElementGuid>6551e4bd-141e-431c-a8ac-871b3c77448e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/input</value>
      <webElementGuid>e10d8516-70dc-4f6a-8946-fbff6c5a42de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'login-button' and @name = 'login-button']</value>
      <webElementGuid>29423bd4-d44a-4ef5-a90a-ae153c72e95e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
