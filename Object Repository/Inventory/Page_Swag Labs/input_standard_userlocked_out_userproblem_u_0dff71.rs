<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_standard_userlocked_out_userproblem_u_0dff71</name>
   <tag></tag>
   <elementGuidId>8434518b-16ee-4da1-b040-c4a45859e66e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='login-button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#login-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3593f9cf-c2f6-4179-8b7a-b6d9aedf3ac2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>cfb36f74-0ec0-41ec-a184-d1b2f6a65de5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button btn_action</value>
      <webElementGuid>bee03dbf-2090-4e2a-b55b-25de608dd8e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>9825ec8d-4cc4-4f99-8ba6-877f709989f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>d11d289e-f68b-4ea3-a639-18fd036790c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>6ea84ecb-a997-4406-b9b9-02608222babb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Login</value>
      <webElementGuid>3e30f7a9-58e6-4f0b-bcef-0f89200a7b75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login-button&quot;)</value>
      <webElementGuid>8238fbf0-8e33-43a1-bcab-0e64313d3e37</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='login-button']</value>
      <webElementGuid>5452db8a-2762-4077-98eb-88175b5f43e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/input</value>
      <webElementGuid>3e6f4f18-2ca1-4a02-912b-657c1aa219dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/input</value>
      <webElementGuid>e7517adf-37d1-4901-90f2-6610d61d4d4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'login-button' and @name = 'login-button']</value>
      <webElementGuid>6f53681c-c988-4c55-b677-60aaf4f4378c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
