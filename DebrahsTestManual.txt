Thank you for visiting my demo repo. 


This webproject assumes the following:
		- User has JAVA installed
		- User has access to Katalon Data Studio
		- User has access to GitLab 
		- Browser of choice : Google Chrome 
		
This automation project examines the website: https://www.saucedemo.com/. This assessment is done for the following scenarios
		- Login
		- Inventory 
		- Customer Cart
		- Customer Checkout
		- Footer Elements
		- Logout 

This project does not take into consideration:
	- Sessions
	- API
	- Layout
	
STEPS TO ACCESS REPO OUTSIDE OF GITLAB:

1. To use Katalon Data Studio - https://katalon.com/download/ it is free, kindly create an account and download the zip folder.
2. Clone the git repository link provided: https://gitlab.com/QualityDARM/RealDecoy-Challenge1.git
3. Select Test Cases or Test Suites and press play.
The test results are reported in the application console

N.B to view execution of test suite in GitLab view results in the CI/CD tab.
Note: the job runs to completion but reports a failure, a bug of gitlab. Click the job to view the console output.