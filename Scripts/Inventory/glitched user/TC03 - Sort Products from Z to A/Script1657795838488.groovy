import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.saucedemo.com/')

WebUI.setText(findTestObject('Object Repository/Inventory/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac (2)'), 
    'performance_glitch_user')

WebUI.setEncryptedText(findTestObject('Object Repository/Inventory/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9 (2)'), 
    'qcu24s4901FyWDTwXGr6XA==')

WebUI.sendKeys(findTestObject('Object Repository/Inventory/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9 (2)'), 
    Keys.chord(Keys.ENTER))

WebUI.selectOptionByValue(findTestObject('Object Repository/Inventory/Page_Swag Labs/select_Name (A to Z)Name (Z to A)Price (low_f7e90a'), 
    'za', true)

WebUI.click(findTestObject('Object Repository/Inventory/Page_Swag Labs/div_This classic Sauce Labs t-shirt is perf_b1833a'))

WebUI.click(findTestObject('Object Repository/Inventory/Page_Swag Labs/div_Test.allTheThings() T-Shirt (Red)This c_d104c4 (1)'))

WebUI.click(findTestObject('Object Repository/Inventory/Page_Swag Labs/div_Sauce Labs Fleece JacketIts not every d_f82aed'))

WebUI.selectOptionByValue(findTestObject('Object Repository/Inventory/Page_Swag Labs/select_Name (A to Z)Name (Z to A)Price (low_f7e90a'), 
    'az', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Inventory/Page_Swag Labs/select_Name (A to Z)Name (Z to A)Price (low_f7e90a'), 
    'za', true)

WebUI.closeBrowser()

