import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.saucedemo.com/')

WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac (5)'), 
    'standard_user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9 (6)'), 
    'qcu24s4901FyWDTwXGr6XA==')

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71 (6)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/button_Add to cart (4)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/a_1 (2)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/button_Checkout (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_Checkout Your Information_firstName (1)'), 'test')

WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_Checkout Your Information_lastName (1)'), 'me')

WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_Checkout Your Information_postalCode (1)'), 'no')

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_Cancel'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/input_Cancel_continue (1)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/button_Finish'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/h2_THANK YOU FOR YOUR ORDER'))

WebUI.rightClick(findTestObject('Object Repository/Page_Swag Labs/h2_THANK YOU FOR YOUR ORDER'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_Your order has been dispatched, and wil_39390f'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Swag Labs/div_Your order has been dispatched, and wil_39390f'), 
    'Your order has been dispatched, and will arrive just as fast as the pony can get there!')

WebUI.closeBrowser()

