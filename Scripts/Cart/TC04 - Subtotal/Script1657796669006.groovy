import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.saucedemo.com/')

WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac (3)'), 
    'performance_glitch_user')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9 (4)'), 
    'qcu24s4901FyWDTwXGr6XA==')

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71 (4)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/button_Add to cart (2)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/button_Add to cart_1 (1)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/button_Add to cart_1_2 (1)'))

WebUI.rightClick(findTestObject('Object Repository/Page_Swag Labs/div_29.99 (1)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_Sauce Labs Bike LightA red light isnt t_ab9835'))

WebUI.rightClick(findTestObject('Object Repository/Page_Swag Labs/div_9.99'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_Sauce Labs Bike LightA red light isnt t_ab9835'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/span_3'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/button_Checkout'))

WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_Checkout Your Information_firstName'), 'Test')

WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_Checkout Your Information_lastName'), 'Ne')

WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_Checkout Your Information_postalCode'), '98989')

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/input_Cancel_continue'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_15.99'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_9.99'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_29.99 (1)'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_Item total 55.97'))

WebUI.click(findTestObject('Object Repository/Page_Swag Labs/div_Total 60.45'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Swag Labs/div_Total 60.45'), 0)

WebUI.closeBrowser()

